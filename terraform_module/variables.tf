variable "namespace" {
  type = string
}

variable "chart_name" {
  type = string
}

variable "license" {
  type    = string
  default = "community"
}

variable "chart_version" {
  type = string
}

variable "values" {
  type    = list(string)
  default = []
}

variable "helm_force_update" {
  type    = bool
  default = false
}

variable "helm_recreate_pods" {
  type    = bool
  default = false
}

variable "helm_cleanup_on_fail" {
  type    = bool
  default = false
}

variable "helm_max_history" {
  type    = number
  default = 0
}

variable "operator_limits_cpu" {
  type    = string
  default = "1100m"
}

variable "operator_limits_memory" {
  type    = string
  default = "1024Mi"
}

variable "operator_requests_cpu" {
  type    = string
  default = "500m"
}

variable "operator_requests_memory" {
  type    = string
  default = "200Mi"
}

variable "operator_watchNamespace" {
  type    = string
  default = "*"
}