resource "helm_release" "mongodb_operator" {
  chart           = "${var.license}-operator"
  repository      = "https://mongodb.github.io/helm-charts"
  name            = var.chart_name
  namespace       = var.namespace
  version         = var.chart_version
  force_update    = var.helm_force_update
  recreate_pods   = var.helm_recreate_pods
  cleanup_on_fail = var.helm_cleanup_on_fail
  max_history     = var.helm_max_history

  values = var.values
  set {
    name  = "operator.watchNamespace"
    value = var.operator_watchNamespace
  }

  dynamic "set" {
    for_each = var.operator_limits_cpu == null ? [] : [var.operator_limits_cpu]
    content {
      name  = "operator.resources.limits.cpu"
      value = var.operator_limits_cpu
    }
  }

  dynamic "set" {
    for_each = var.operator_limits_memory == null ? [] : [var.operator_limits_memory]
    content {
      name  = "operator.resources.limits.memory"
      value = var.operator_limits_memory
    }
  }

  dynamic "set" {
    for_each = var.operator_requests_cpu == null ? [] : [var.operator_requests_cpu]
    content {
      name  = "operator.resources.requests.cpu"
      value = var.operator_requests_cpu
    }
  }

  dynamic "set" {
    for_each = var.operator_requests_memory == null ? [] : [var.operator_requests_memory]
    content {
      name  = "operator.resources.requests.memory"
      value = var.operator_requests_memory
    }
  }
}
